﻿using UnityEngine;

namespace Client.Scripts.Effects.Pixelation
{
    [ExecuteInEditMode]
    public class PixelationEffect : MonoBehaviour
    {
        public Material effectMaterial;

        private void OnRenderImage(RenderTexture src, RenderTexture dest)
        {
            Graphics.Blit(src, dest, effectMaterial);
        }
    }
}
