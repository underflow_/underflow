﻿using UnityEngine;

namespace Client.Scripts.Context
{
    public enum CameraPreserveType
    {
        Secondary = 0,
        Primary,
    }

    public class PreserveCamera : MonoBehaviour
    {
        public static PreserveCamera Instance { get; private set; } = null;

        public CameraPreserveType type;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
            {
                if (Instance.type == CameraPreserveType.Secondary && type == CameraPreserveType.Secondary)
                    SaveOriginal();
                else if (Instance.type == CameraPreserveType.Secondary && type == CameraPreserveType.Primary)
                    DestroyOriginal();
                else if (Instance.type == CameraPreserveType.Primary && type == CameraPreserveType.Secondary)
                    SaveOriginal();
                else if (Instance.type == CameraPreserveType.Primary && type == CameraPreserveType.Primary)
                    DestroyOriginal();
            }
        }

        private void DestroyOriginal()
        {
            foreach (var otherCamera in FindObjectsOfType<Camera>())
            {
                if (otherCamera.transform.GetComponent<PreserveCamera>() != this)
                {
                    Destroy(otherCamera.gameObject);
                }
            }

            Instance = this;
        }

        private void SaveOriginal()
        {
            Destroy(gameObject);
        }
    }
}