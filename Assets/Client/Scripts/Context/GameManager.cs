﻿using Client.Scripts.Music;
using Client.Scripts.Scenes.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Client.Scripts.Context
{
    public class GameManager : MonoBehaviour
    {
        #region Singleton

        public static GameManager Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);

            DontDestroyOnLoad(this);
        }

        #endregion

        private void Start()
        {
            Settings.ApplyCurrentSettings();
        }

        #region LoadScene

        public void LoadGameScene(bool isTutorial)
        {
            MusicController.Instance.SetTrack(isTutorial ? TrackType.TutorialTheme : TrackType.GameTheme);
            SceneManager.LoadScene("GameScene");
            Time.timeScale = 1.0f;
            GameSceneManager.Instance.isTutorial = isTutorial;
        }

        public void LoadPauseMenuScene()
        {
            SceneManager.LoadScene("PauseMenuScene", LoadSceneMode.Additive);
        }

        public void LoadSettingsScene()
        {
            SceneManager.LoadScene("SettingsMenuScene", LoadSceneMode.Additive);
        }

        public void LoadMainMenuScene()
        {
            MusicController.Instance.SetTrack(TrackType.MainMenuTheme);
            SceneManager.LoadScene("MainMenuScene");
        }

        public void LoadDefeatScene()
        {
            MusicController.Instance.SetTrack(TrackType.GameTheme);
            SceneManager.LoadScene("DefeatScene", LoadSceneMode.Additive);
        }

        public void LoadVictoryScene()
        {
            MusicController.Instance.SetTrack(TrackType.GameTheme);
            SceneManager.LoadScene("VictoryScene", LoadSceneMode.Additive);
        }

        public void LoadCreditsScene()
        {
            MusicController.Instance.SetTrack(TrackType.FinalTheme);
            SceneManager.LoadScene("CreditsScene", LoadSceneMode.Single);
        }

        #endregion
    }
}