﻿using System;
using Client.Scripts.Music;
using UnityEngine;

namespace Client.Scripts.Context
{
    public struct SettingsValues
    {
        public bool MusicOn;
        public float MusicVolume;
        public bool SoundOn;
        public float SoundVolume;
    }

    public class Settings
    {
        private static readonly Lazy<Settings> Instance = new Lazy<Settings>(() => new Settings());

        public static Settings Current => Instance.Value;

        // Музыка включена
        private const string MusicOnParameterName = "Settings_Parameter_MusicOn";

        public bool MusicOn { get; private set; }

        // Громкость музыки
        private const string MusicVolumeParameterName = "Settings_Parameter_MusicVolume";
        public float MusicVolume { get; private set; }

        // Звук включён
        public bool SoundOn { get; private set; }
        private const string SoundOnParameterName = "Settings_Parameter_SoundOn";

        // Громкость звука
        private const string SoundVolumeParameterName = "Settings_Parameter_SoundVolume";
        public float SoundVolume { get; private set; }

        public SettingsValues GetSettings()
        {
            return new SettingsValues
            {
                MusicOn = MusicOn,
                MusicVolume = MusicVolume,
                SoundOn = SoundOn,
                SoundVolume = SoundVolume,
            };
        }

        private Settings()
        {
            MusicOn = Convert.ToBoolean(PlayerPrefs.GetInt(MusicOnParameterName, 1));
            MusicVolume = PlayerPrefs.GetFloat(MusicVolumeParameterName, 1.0f);
            SoundOn = Convert.ToBoolean(PlayerPrefs.GetInt(SoundOnParameterName, 1));
            SoundVolume = PlayerPrefs.GetFloat(SoundVolumeParameterName, 1.0f);
        }

        public void Save(SettingsValues values)
        {
            MusicOn = values.MusicOn;
            MusicVolume = values.MusicVolume;
            SoundOn = values.SoundOn;
            SoundVolume = values.SoundVolume;

            Save();
        }

        private void Save()
        {
            PlayerPrefs.SetInt(MusicOnParameterName, Convert.ToInt32(MusicOn));
            PlayerPrefs.SetFloat(MusicVolumeParameterName, MusicVolume);
            PlayerPrefs.SetInt(SoundOnParameterName, Convert.ToInt32(SoundOn));
            PlayerPrefs.SetFloat(SoundVolumeParameterName, SoundVolume);

            PlayerPrefs.Save();
        }

        public static void ApplySettings(SettingsValues values)
        {
            MusicController.Instance.ChangeState(values.MusicOn);
            MusicController.Instance.ChangeVolume(values.MusicVolume);
        }

        public static void ApplyCurrentSettings()
        {
            ApplySettings(Current.GetSettings());
        }
    }
}