﻿using System;
using System.Collections.Generic;
using Client.Scripts.Context;
using Client.Scripts.Scenes.Game;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Scenes.Defeat
{
    [Serializable]
    public class ResultDataEntry
    {
        public Sprite icon;
        public string text;
        public uint amount;
    }

    public class DefeatSceneManager : MonoBehaviour
    {
        [Header("Data")]
        public GameObject resultPrefab;

        public Transform resultsHolder;

        public List<ResultDataEntry> resultData = new List<ResultDataEntry>();

        private void Start()
        {
            foreach (var resultDataEntry in resultData)
            {
                var go = Instantiate(resultPrefab, resultsHolder);
                go.transform.Find("IconImage").GetComponent<Image>().sprite = resultDataEntry.icon;
                go.transform.Find("NameText").GetComponent<Text>().text = resultDataEntry.text;
                go.transform.Find("CountText").GetComponent<Text>().text = resultDataEntry.amount.ToString();
            }
        }

        #region Buttons

        public void OnMenuButtonClick()
        {
            GameManager.Instance.LoadMainMenuScene();
        }

        public void OnRetryButtonClick()
        {
            if (GameSceneManager.Instance.isTutorial)
                GameManager.Instance.LoadGameScene(true);
            else
                GameManager.Instance.LoadCreditsScene();
        }

        #endregion
    }
}