using System;
using System.Collections.Generic;
using Client.Scripts.Context;
using Client.Scripts.Scenes.Game;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Client.Scripts.Scenes.Victory
{
    public enum ResourceType
    {
        Undefined = 0,
        TorpedoTypeA,
        TorpedoTypeB,
        TorpedoTypeC,
    }

    [Serializable]
    public class ResourceSpriteEntry
    {
        public ResourceType type;
        public Sprite sprite;
    }

    [Serializable]
    public class ResourceDataEntry
    {
        public ResourceType type;
        public uint amount;
    }

    public class VictorySceneManager : MonoBehaviour
    {
        [Header("Data")] public GameObject resourcePrefab;

        public Transform resourcesHolder;

        public List<ResourceDataEntry> resourceData = new List<ResourceDataEntry>();

        [Header("Visualization")] public List<ResourceSpriteEntry> resourceSprites = new List<ResourceSpriteEntry>();

        private void Start()
        {
            foreach (var resourceDataEntry in resourceData)
            {
                var go = Instantiate(resourcePrefab, resourcesHolder);
                go.transform.Find("NameText").GetComponent<Text>().text = resourceDataEntry.type.ToString();
                go.transform.Find("AmountText").GetComponent<Text>().text = resourceDataEntry.amount.ToString();
                var sprite = resourceSprites.Find(entry => entry.type == resourceDataEntry.type).sprite;
                if (sprite != null)
                    go.transform.Find("IconImage").GetComponent<Image>().sprite = sprite;
            }
        }


        #region Buttons

        public void OnMenuButtonClick()
        {
            GameManager.Instance.LoadMainMenuScene();
        }

        public void OnContinueButtonClick()
        {
            SceneManager.UnloadSceneAsync("VictoryScene");
            SceneManager.sceneUnloaded += OnVictorySceneUnloaded;
        }

        private void OnVictorySceneUnloaded(Scene scene)
        {
            SceneManager.sceneUnloaded -= OnVictorySceneUnloaded;
            FindObjectOfType<GameSceneManager>().Mode = GameMode.Sailing;
        }

        #endregion
    }
}