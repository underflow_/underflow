﻿using System.Collections;
using Client.Scripts.Context;
using Client.Scripts.Scenes.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Client.Scripts.Scenes.Menus
{
    public class MainMenuManager : MonoBehaviour
    {
        public GameObject canvas;

        private void Awake()
        {
            StartCoroutine(LoadYourAsyncScene());
        }

        private IEnumerator LoadYourAsyncScene()
        {
            var asyncLoad = SceneManager.LoadSceneAsync("GameScene", LoadSceneMode.Additive);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            FindObjectOfType<CameraController>().viewMode = CameraViewMode.Disabled;
            GameSceneManager.Instance.Mode = GameMode.Menu;
            Camera.main.transform.Translate(-10, 0, -100);
        }

        #region Menu

        public void OnNewGameClick()
        {
            GameManager.Instance.LoadGameScene(true);
        }

        public void OnContinueClick()
        {
            GameManager.Instance.LoadGameScene(false);
        }

        public void OnCreditsButtonClick()
        {
            GameManager.Instance.LoadCreditsScene();
        }

        public void OnSettingsClick()
        {
            canvas.SetActive(false);
            SceneManager.LoadScene("SettingsMenuScene", LoadSceneMode.Additive);
        }

        public void OnExitClick()
        {
            Application.Quit();
        }

        #endregion

        /// <summary>
        /// Действия при возвращении на сцену
        /// </summary>
        public void Activate()
        {
            canvas.SetActive(true);
        }
    }
}