﻿using Client.Scripts.Context;
using Client.Scripts.Scenes.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Client.Scripts.Scenes.Menus
{
    public class PauseMenuSceneManager : MonoBehaviour
    {
        public GameObject canvas;

        #region Buttons

        public void OnContinueButtonClick()
        {
            SceneManager.UnloadSceneAsync("PauseMenuScene");
            SceneManager.sceneUnloaded += OnPauseMenuSceneUnloaded;
        }

        private void OnPauseMenuSceneUnloaded(Scene scene)
        {
            SceneManager.sceneUnloaded -= OnPauseMenuSceneUnloaded;
            FindObjectOfType<GameSceneManager>().Mode = GameMode.Fight;
        }

        public void OnSettingsButtonClick()
        {
            canvas.SetActive(false);
            GameManager.Instance.LoadSettingsScene();
        }

        public void OnMainMenuButtonClick()
        {
            GameManager.Instance.LoadMainMenuScene();
        }

        #endregion

        /// <summary>
        /// Действия при возвращении на сцену
        /// </summary>
        public void Activate()
        {
            canvas.SetActive(true);
        }
    }
}