﻿using Client.Scripts.Context;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Client.Scripts.Scenes.Menus
{
    public class SettingsMenuManager : MonoBehaviour
    {
        [Header("UI")] public Toggle musicOnToggle;

        public Slider musicVolumeSlider;
        public Toggle soundOnToggle;
        public Slider soundVolumeSlider;

        private SettingsValues _originalSettingsValues;
        private SettingsValues _updatedSettingsValues;

        private void Start()
        {
            Settings.ApplySettings(Settings.Current.GetSettings());
        }

        private void Awake()
        {
            // Загрузка текущих значений
            musicOnToggle.isOn = Settings.Current.MusicOn;
            musicVolumeSlider.value = Settings.Current.MusicVolume;
            soundOnToggle.isOn = Settings.Current.SoundOn;
            soundVolumeSlider.value = Settings.Current.SoundVolume;

            // Задание реакций на изменение значений интерфейса
            musicOnToggle.onValueChanged.AddListener(delegate { OnMusicOnToggleValueChanged(); });
            musicVolumeSlider.onValueChanged.AddListener(delegate { OnMusicVolumeSliderValueChanged(); });
            soundOnToggle.onValueChanged.AddListener(delegate { OnSoundOnToggleValueChanged(); });
            soundVolumeSlider.onValueChanged.AddListener(delegate { OnSoundVolumeSliderValueChanged(); });

            // Полчение значений для отката
            _originalSettingsValues = Settings.Current.GetSettings();
            _updatedSettingsValues = Settings.Current.GetSettings();
        }

        #region Menu

        public void OnSaveClick()
        {
            if (!Settings.Current.GetSettings().Equals(_updatedSettingsValues))
                Settings.Current.Save(_updatedSettingsValues);

            UnloadScene();
        }

        public void OnCancelClick()
        {
            if (!Settings.Current.GetSettings().Equals(_originalSettingsValues))
                Settings.Current.Save(_originalSettingsValues);

            Settings.ApplySettings(_originalSettingsValues);

            UnloadScene();
        }

        private void UnloadScene()
        {
            SceneManager.UnloadSceneAsync("SettingsMenuScene");
            SceneManager.sceneUnloaded += OnSettingsMenuSceneUnloaded;
        }

        private void OnSettingsMenuSceneUnloaded(Scene scene)
        {
            SceneManager.sceneUnloaded -= OnSettingsMenuSceneUnloaded;

            // Если предыдущей сценой было главное меню
            FindObjectOfType<MainMenuManager>()?.Activate();
            // Если предыдущей сценой было меню паузы
            FindObjectOfType<PauseMenuSceneManager>()?.Activate();
        }

        private void OnMusicOnToggleValueChanged()
        {
            _updatedSettingsValues.MusicOn = musicOnToggle.isOn;
            Settings.ApplySettings(_updatedSettingsValues);
        }

        private void OnMusicVolumeSliderValueChanged()
        {
            _updatedSettingsValues.MusicVolume = musicVolumeSlider.value;
            Settings.ApplySettings(_updatedSettingsValues);
        }

        private void OnSoundOnToggleValueChanged()
        {
            _updatedSettingsValues.SoundOn = soundOnToggle.isOn;
        }

        private void OnSoundVolumeSliderValueChanged()
        {
            _updatedSettingsValues.SoundVolume = soundVolumeSlider.value;
        }

        #endregion
    }
}