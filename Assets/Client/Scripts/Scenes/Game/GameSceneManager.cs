﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Client.Scripts.Context;
using Client.Scripts.Scenes.Game.Model;
using Client.Scripts.Scenes.Game.Submarine;
using Client.Scripts.Scenes.Game.Submarine.Sonar;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace Client.Scripts.Scenes.Game
{
    public enum GameMode
    {
        Sailing,
        Fight,
        AfterFight,
        Menu,
    }

    public class GameSceneManager : MonoBehaviour
    {
        #region Singleton

        public static GameSceneManager Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        public bool isTutorial = true;

        private const int TutorialChance = 100;
        private const int GameChance = 25;

        private int _torpedoChance = 100;

        public GameObject gameCanvas;
        public GameObject ammunitionPanel;
        public Button continueButton;

        private SonarController _sonarController;

        [Header("Enemies")]
        public float enemyUpdateFrequency = 0.1f;

        private float _lastEnemyUpdateTime;

        // Список всех внехних игры
        private readonly List<WorldObject> _objects = new List<WorldObject>();

        // Текущее состояние игры

        public bool _isRunning = true;

        public bool IsRunning
        {
            get => _isRunning;
            set => _isRunning = value;
        }

        // Текущий режим игры
        private GameMode _mode = GameMode.Sailing;

        public GameMode Mode
        {
            get => _mode;
            set
            {
                _mode = value;
                switch (_mode)
                {
                    case GameMode.Menu:
                        IsRunning = false;
                        Time.timeScale = 0.0f;
                        gameCanvas.SetActive(false);
                        break;
                    case GameMode.Sailing:
                        IsRunning = true;
                        Time.timeScale = 1.0f;
                        gameCanvas.SetActive(true);
                        ammunitionPanel.SetActive(false);
                        break;
                    case GameMode.Fight:
                        IsRunning = true;
                        Time.timeScale = 1.0f;
                        gameCanvas.SetActive(true);
                        ammunitionPanel.SetActive(true);
                        break;
                    case GameMode.AfterFight:
                        IsRunning = false;
                        Time.timeScale = 0.0f;
                        gameCanvas.SetActive(false);
                        continueButton.enabled = true;
                        break;
                    default:
                        Debug.LogError("Game mode is undefined");
                        break;
                }
            }
        }

        private RoomManager _roomManager;

        private readonly Random _random = new Random();

        private void Start()
        {
            _roomManager = FindObjectOfType<RoomManager>();
            _sonarController = FindObjectOfType<SonarController>();

            ammunitionPanel.SetActive(false);

            _objects.Add(new PlayerObject
            {
                Id = Guid.NewGuid(),
                Position = Vector2.zero
            });

            _torpedoChance = isTutorial ? TutorialChance : GameChance;

            StartCoroutine(StartFightDelayed(2f));
        }

        private IEnumerator StartFightDelayed(float time)
        {
            yield return new WaitForSeconds(time);
            StartFight();
        }

        private void StartFight()
        {
            Mode = GameMode.Fight;

            // TODO: Добавить правило генерации врага
            var position = new Vector2(3.0f, 3.0f);

            _objects.Add(new EnemyObject
            {
                Id = Guid.NewGuid(),
                Position = position,
                Rotation = (GetWorldObject<PlayerObject>().Position - position).normalized,
                Speed = 0.0f,
                Name = "Cruel Enemy",
                Health = 100,
                Ammunition = new Dictionary<TorpedoType, uint>
                {
                    [TorpedoType.Basic] = 5,
                    [TorpedoType.Advanced] = 2,
                    [TorpedoType.Ultra] = 0,
                }
            });
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && Mode != GameMode.Menu)
                OnPauseMenuButtonClick();
        }

        private void FixedUpdate()
        {
            // Обработать состояния существующих мировых объектов
            foreach (var worldObject in _objects.ToArray())
            {
                // Обновление данных объекта на сонаре
                if (worldObject.GetType() == typeof(PlayerObject))
                {
                    _sonarController.UpdatePlayerPosition(worldObject.Position);
                }
                else
                {
                    var sonarObject = new SonarObject
                    {
                        Position = worldObject.Position,
                        Rotation = worldObject.Rotation,
                    };
                    if (worldObject.GetType() == typeof(TorpedoObject))
                    {
                        sonarObject.Type = ((TorpedoObject) worldObject).OwnerId == GetWorldObject<PlayerObject>().Id
                            ? SonarObjectType.FriendlyTorpedo
                            : SonarObjectType.Torpedo;
                    }
                    else if (worldObject.GetType() == typeof(EnemyObject))
                        sonarObject.Type = SonarObjectType.Submarine;

                    _sonarController.AddObject(worldObject.Id, sonarObject);
                }
            }

            // Принять решения о действиях противников
            if (Time.time > _lastEnemyUpdateTime + enemyUpdateFrequency)
            {
                _lastEnemyUpdateTime = Time.time;

                foreach (var worldObject in _objects.ToArray())
                {
                    if (worldObject.GetType() == typeof(EnemyObject))
                    {
                        // Заупуск торпеды
                        var chance = UnityEngine.Random.Range(0, _torpedoChance);
                        if (chance == 0)
                        {
                            _objects.Add(new TorpedoObject()
                            {
                                Id = Guid.NewGuid(),
                                Position = worldObject.Position,
                                Rotation = (GetWorldObject<PlayerObject>().Position - worldObject.Position).normalized,
                                Speed = 0.25f,
                                Type = TorpedoType.Basic,
                                OwnerId = worldObject.Id,
                                Visualized = false,
                            });
                        }
                    }
                }
            }

            // Обработать состояния существующих мировых объектов
            foreach (var worldObject in _objects.ToArray())
            {
                // Обновление координаты объекта
                worldObject.Position = worldObject.Position +
                                       worldObject.Rotation.normalized * worldObject.Speed * Time.deltaTime;

                // Проверка попаданий
                if (worldObject.GetType() == typeof(TorpedoObject))
                {
                    var torpedoObject = (TorpedoObject) worldObject;

                    // Если торпеда уже была визализирована
                    if (torpedoObject.Visualized)
                        continue;

                    // Сравнить все другие объекты
                    foreach (var obj in _objects
                        .Where(_ => _.GetType().IsSubclassOf(typeof(SubmarineObject))).ToArray())
                    {
                        var submarineObject = (SubmarineObject) obj;
                        // Если попали не во владельца
                        if (torpedoObject.OwnerId != submarineObject.Id)
                        {
                            var torpedoDamageRadius = ((int) torpedoObject.Type + 1) * 0.25f;

                            // Проверка попдания
                            if (PointInCircle(torpedoObject.Position, torpedoDamageRadius, submarineObject.Position))
                            {
                                torpedoObject.Visualized = true;

                                if (submarineObject.GetType() == typeof(EnemyObject))
                                {
                                    // Удалить торпеду
                                    RemoveObject(torpedoObject.Id);
                                    // Нанести урон врагу
                                    var damage = (uint) (torpedoObject.Type + 1) * 25;
                                    submarineObject.Health -= damage;
                                    if (submarineObject.Health <= 0)
                                    {
                                        // Убить противника
                                        RemoveObject(submarineObject.Id);
                                        GameManager.Instance.LoadVictoryScene();
                                        Mode = GameMode.AfterFight;
                                    }
                                }

                                if (submarineObject.GetType() == typeof(PlayerObject))
                                {
                                    // Проверить шанс попадания
                                    if (!Convert.ToBoolean(UnityEngine.Random.Range(0, 2)))
                                    {
                                        AmmunitionManager.Instance.SpawnEnemyTorpedoMiss();
                                    }
                                    else
                                    {
                                        // Удалить торпеду
                                        RemoveObject(torpedoObject.Id);

                                        // Выбор комнаты для попадания
                                        var roomControllers = _roomManager.roomControllers;
                                        var roomController =
                                            roomControllers[_random.Next(roomControllers.Count)];

                                        // Запустить визуализацию
                                        AmmunitionManager.Instance.SpawnEnemyTorpedoHit(roomController);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void RemoveObject(Guid id)
        {
            _objects.Remove(_objects.Find(worldObject => id == worldObject.Id));
            _sonarController.RemoveObject(id);
        }

        /// <summary>
        /// Запуск торпеды
        /// </summary>
        /// <param name="type">Тип торпеды</param>
        public void ShootTorpedo(TorpedoType type)
        {
            var player = GetWorldObject<PlayerObject>();
            var enemy = GetWorldObject<EnemyObject>();

            if (player == null || enemy == null)
            {
                Debug.LogWarning("Could not shoot a torpedo");
                return;
            }

            _objects.Add(new TorpedoObject
            {
                Id = Guid.NewGuid(),
                Position = player.Position,
                Rotation = enemy.Position - player.Position,
                Speed = 0.25f * ((int) type + 1),
                Type = type,
                OwnerId = player.Id,
            });
        }

        private T GetWorldObject<T>() where T : WorldObject
        {
            foreach (var worldObject in _objects)
            {
                if (worldObject.GetType() == typeof(T))
                {
                    return worldObject as T;
                }
            }

            Debug.LogWarning($"Type {typeof(T)} was not found");
            return null;
        }

        /// <summary>
        /// Проверка попадания точки в окружность
        /// </summary>
        /// <param name="circlePosition">Позиция центра окружности</param>
        /// <param name="circleRadius">Радиус окружности</param>
        /// <param name="pointPosition">Позиция точки</param>
        /// <returns>Нахождение точки внутри окружности</returns>
        private static bool PointInCircle(Vector2 circlePosition, float circleRadius, Vector2 pointPosition)
        {
            return Math.Pow(pointPosition.x - circlePosition.x, 2) + Math.Pow(pointPosition.y - circlePosition.y, 2) <
                   Math.Pow(circleRadius, 2);
        }

        #region Buttons

        public void OnPauseMenuButtonClick()
        {
            Mode = GameMode.Menu;
            GameManager.Instance.LoadPauseMenuScene();
        }

        public void OnContinueButtonClick()
        {
            GameManager.Instance.LoadGameScene(false);
        }

        #endregion
    }
}