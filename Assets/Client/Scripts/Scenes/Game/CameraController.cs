﻿using UnityEngine;

namespace Client.Scripts.Scenes.Game
{
    public enum CameraViewMode
    {
        Submarine,
        Disabled,
    }

    [AddComponentMenu("Camera/CameraController")]
    [RequireComponent(typeof(Camera))]
    [SelectionBase]
    public class CameraController : MonoBehaviour
    {
        public CameraViewMode viewMode = CameraViewMode.Submarine;

        [Tooltip("Use initial position from the editor")]
        public bool useInitialPosition = false;

        #region zoom

        [Header("Zoom")]
        public Transform target;

        public float zoomSpeed = 1;
        public float zoomDampening = 5.0f;

        public float initialDistance = 5.0f;
        public float minDistance = 0.6f;
        public float maxDistance = 20.0f;

        private float _currentDistance;
        private float _desiredDistance;

        #endregion

        #region drag

        [Header("Drag")]
        public float dragSpeed = 0.5f;

        public Vector2 offsetPos = Vector2.zero;

        public Vector2 minPos = new Vector2(-100, -100);
        public Vector2 maxPos = new Vector2(100, 100);

        private Vector2 _currentPos;
        private Vector2 _desiredPos;

        #endregion

        private GameObject _currentCrew;
        private Vector3 _previousMousePosition;

        private void Start()
        {
            _currentDistance = initialDistance;
            _desiredDistance = initialDistance;

            _currentPos = offsetPos;
            _desiredPos = offsetPos;
        }

        private bool _wasMoved = false;

        public bool WasMoved
        {
            get
            {
                if (_wasMoved)
                {
                    _wasMoved = false;
                    return true;
                }

                return false;
            }
        }

        private void Update()
        {
            if (viewMode == CameraViewMode.Disabled)
                return;

            _desiredDistance -= Input.mouseScrollDelta.y * Time.deltaTime * zoomSpeed * Mathf.Abs(_desiredDistance);
            _desiredDistance = Mathf.Clamp(_desiredDistance, minDistance, maxDistance);
            _currentDistance = Mathf.Lerp(_currentDistance, _desiredDistance, Time.deltaTime * zoomDampening);

            if (Input.GetMouseButtonDown(0))
            {
                _previousMousePosition = Input.mousePosition;
            }

            if (Input.GetMouseButton(0))
            {
                _wasMoved = _wasMoved || Vector3.Distance(Input.mousePosition, _previousMousePosition) > 3;

                var mouseDelta = Input.mousePosition - _previousMousePosition;
                _desiredPos -= (Vector2) mouseDelta * dragSpeed * Time.deltaTime *
                               _currentDistance / minDistance;
                Vector2.Lerp(_currentPos, _desiredPos, Time.deltaTime);
                _desiredPos = new Vector2(
                    Mathf.Clamp(_desiredPos.x, minPos.x, maxPos.x),
                    Mathf.Clamp(_desiredPos.y, minPos.y, maxPos.y));
                _currentPos = _desiredPos;

                _previousMousePosition = Input.mousePosition;
            }

            transform.position = target.position +
                                 _currentDistance * Vector3.back +
                                 _currentPos.x * Vector3.right +
                                 _currentPos.y * Vector3.up;
        }
    }
}