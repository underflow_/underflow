using System.Collections.Generic;
using Client.Scripts.Scenes.Game.Submarine;

namespace Client.Scripts.Scenes.Game.Model
{
    public class SubmarineObject : WorldObject
    {
        public uint Health;
        public Dictionary<TorpedoType, uint> Ammunition;
    }
}