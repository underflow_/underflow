namespace Client.Scripts.Scenes.Game.Model
{
    public class EnemyObject : SubmarineObject
    {
        public string Name;

        public uint ReceiveDamage(uint amount)
        {
            Health -= amount;
            return Health;
        }
    }
}