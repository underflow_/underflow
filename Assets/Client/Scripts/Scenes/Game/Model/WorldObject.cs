using System;
using UnityEngine;

namespace Client.Scripts.Scenes.Game.Model
{
    public class WorldObject
    {
        public Guid Id;
        public Vector2 Position;
        public Vector2 Rotation;
        public float Speed;
    }
}