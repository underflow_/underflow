using System;
using Client.Scripts.Scenes.Game.Submarine;

namespace Client.Scripts.Scenes.Game.Model
{
    public class TorpedoObject : WorldObject
    {
        public Guid OwnerId;
        public TorpedoType Type;
        public bool Visualized;
    }
}