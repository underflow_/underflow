﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Scenes.Game.Submarine
{
    public enum RoomState
    {
        Stable = 0,
        Broken,
        OnFire,
        Flooded
    }

    public enum RoomType
    {
        Undefined = 0,
        Engine,
        Energy,
        Com,
        Living,
        Torpedo,
        Battery,
        Med,
        Sonar,
        Fuel,
        Storage,
    }

    [Serializable]
    public class RoomData
    {
        public string name;
        public RoomState state;
        public RoomController roomController;
        public Sprite icon;
        public RoomType type;
    }

    public class RoomManager : MonoBehaviour
    {
        [Header("Data")]
        public List<RoomData> roomData = new List<RoomData>();

        [HideInInspector]
        public List<RoomController> roomControllers;

        private RoomController _selectedRoom;

        public RoomController GetRoomOfType(RoomType type)
        {
            foreach (var roomController in roomControllers)
                if (roomController.type == type)
                    return roomController;

            return null;
        }

        #region RoomSelection

        [Header("Selection")]
        public float timeBetweenClicks = 0.2f;

        private float _firstClickTime;
        private bool _isCoroutineAllowed = true;
        private int _clickCounter = 0;
        private bool _handleDoubleClick = false;

        public GameObject roomInfoPanel;
        private Text _nameText;
        private Image _iconImage;

        #endregion

        private Camera _camera;

        private void Start()
        {
            _camera = Camera.main;

            if (roomInfoPanel == null)
                Debug.LogError("Room info panel is not assigned");

            roomInfoPanel.transform.Find("Image").Find("Icon").GetComponent<Image>();
            _nameText = roomInfoPanel.transform.Find("NameText").GetComponent<Text>();
            _iconImage = roomInfoPanel.transform.Find("Image").Find("Icon").GetComponent<Image>();

            roomControllers = new List<RoomController>(FindObjectsOfType<RoomController>());

            foreach (var room in roomData)
            {
                var roomController = roomControllers.Find(controller => controller == room.roomController);
                if (roomController == null)
                {
                    Debug.LogWarning($"Room {room.name} not found");
                    continue;
                }

                roomController.Initialize(room);
            }

            roomInfoPanel.SetActive(false);
        }

        private void Update()
        {
            if (Input.GetMouseButtonUp(0))
                _clickCounter++;
            if (_clickCounter == 1 && _isCoroutineAllowed)
            {
                _firstClickTime = Time.time;
                StartCoroutine(DoubleClickDetection());
            }

            if (_handleDoubleClick)
            {
                _handleDoubleClick = false;

                var ray = _camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out var hit))
                {
                    if (hit.collider.CompareTag("Room"))
                    {
                        var roomController = hit.collider.transform.GetComponent<RoomController>();
                        if (roomController == _selectedRoom)
                            UnselectRoom();
                        else
                            SelectRoom(roomController);
                    }
                    else
                        UnselectRoom();
                }
            }

            if (FindObjectOfType<CameraController>().WasMoved)
            {
                UnselectRoom();
            }
        }

        private IEnumerator DoubleClickDetection()
        {
            _isCoroutineAllowed = false;
            while (Time.time < _firstClickTime + timeBetweenClicks)
            {
                if (_clickCounter == 2)
                {
                    _handleDoubleClick = true;
                    break;
                }

                yield return new WaitForEndOfFrame();
            }

            _clickCounter = 0;
            _isCoroutineAllowed = true;
        }

        private void SelectRoom(RoomController roomController)
        {
            _selectedRoom = roomController;
            _nameText.text = _selectedRoom.Name;
            _iconImage.sprite = _selectedRoom.Icon;
            roomInfoPanel.SetActive(true);
        }

        private void UnselectRoom()
        {
            _selectedRoom = null;
            roomInfoPanel.SetActive(false);
        }

        public void MuteAudio()
        {
            roomControllers.ForEach(room => room.MuteAudio());
        }

        public void UnmuteAudio()
        {
            roomControllers.ForEach(room => room.UnmuteAudio());
        }
    }
}