using System;
using System.Collections;
using System.Collections.Generic;
using Client.Scripts.Scenes.Game.Environment;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Client.Scripts.Scenes.Game.Submarine
{
    public enum TorpedoType
    {
        Basic,
        Advanced,
        Ultra
    }

    [Serializable]
    public class TorpedoSprite
    {
        public TorpedoType type;
        public Sprite sprite;
    }

    [Serializable]
    public class TorpedoParameters
    {
        public TorpedoType type;
        public uint speed;
        public uint damage;
        public float reloadSpeed;
    }

    [Serializable]
    public class TorpedoTubeData
    {
        public TorpedoType type;
        public float reloadPercentage = 1f;
    }

    public class AmmunitionManager : MonoBehaviour
    {
        public static AmmunitionManager Instance { get; private set; } = null;

        [Range(0.0f, float.MaxValue)]
        public float torpedoReloadTimeDamp = 1.0f;

        public List<TorpedoParameters> torpedoParameters = new List<TorpedoParameters>();

        private readonly Dictionary<TorpedoType, TorpedoParameters> _torpedoParameters =
            new Dictionary<TorpedoType, TorpedoParameters>();

        [Header("UI")]
        public GameObject torpedoTubePrefab;

        public RectTransform ammunitionPanel;

        public List<TorpedoSprite> torpedoSprites = new List<TorpedoSprite>();

        private readonly List<GameObject> _torpedoTubeGameObjects = new List<GameObject>();

        [Header("Submarine")]
        public List<TorpedoTubeData> torpedoTubes = new List<TorpedoTubeData>();

        [Header("Visuals")]
        public Transform torpedoHolder;

        public GameObject torpedoPrefab;
        public Transform[] torpedoStartPoint;

        private ResourcesManager _resourcesManager;

        public Collider submarineCollider;
        public Transform environmentHolder;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        private void Start()
        {
            submarineCollider = GetComponent<Collider>();
            _resourcesManager = FindObjectOfType<ResourcesManager>();

            // Преобрабразовать данные о видах торпед
            torpedoParameters.ForEach(parameter => { _torpedoParameters.Add(parameter.type, parameter); });

            var ammunitionData = _resourcesManager.ammunitionData;
            foreach (var tube in torpedoTubes)
            {
                var tubeGo = Instantiate(torpedoTubePrefab, ammunitionPanel);
                var index = torpedoTubes.IndexOf(tube);
                tubeGo.name = $"TorpedoTube_{index}";
                var content = tubeGo.transform.Find("Content");
                content.Find("TorpedoButton").Find("Status").GetComponent<Image>().fillAmount = 0f;
                var image = content.transform.Find("TorpedoButton").Find("Background").Find("Image")
                    .GetComponent<Image>();
                content.Find("AmountPanel").Find("Amount").GetComponent<Text>().text =
                    ammunitionData.Find(_ => _.type == tube.type).amount.ToString();
                switch (tube.type)
                {
                    case TorpedoType.Basic:
                        image.sprite = torpedoSprites.Find(sprite => sprite.type == TorpedoType.Basic).sprite;
                        break;
                    case TorpedoType.Advanced:
                        image.sprite = torpedoSprites.Find(sprite => sprite.type == TorpedoType.Advanced).sprite;
                        break;
                    case TorpedoType.Ultra:
                        image.sprite = torpedoSprites.Find(sprite => sprite.type == TorpedoType.Ultra).sprite;
                        break;
                    default:
                        Debug.LogError("Torpedo tube type is not defined");
                        break;
                }

                content.GetComponent<Button>().onClick.AddListener(() => { ShootTorpedo(index); });
                _torpedoTubeGameObjects.Insert(torpedoTubes.IndexOf(tube), tubeGo);
            }
        }

        /// <summary>
        /// Запуск торпеды
        /// </summary>
        /// <param name="tubeNumber">Норер торпедного аппарата</param>
        private void ShootTorpedo(int tubeNumber)
        {
            // Проверка возможности использования торпед данного типа
            var type = torpedoTubes[tubeNumber].type;
            if (!_resourcesManager.UseTorpedo(type))
                return;

            torpedoTubes[tubeNumber].reloadPercentage = 0f;
            var tubeGo = _torpedoTubeGameObjects[tubeNumber];
            var content = tubeGo.transform.Find("Content");
            // Уменьшить отображаемое значение
            content.Find("AmountPanel").Find("Amount").GetComponent<Text>().text =
                _resourcesManager.GetTorpedoes(type).ToString();
            // Запустить анимацию перезарядки и заблокировать кнопку
            var reloadImage = content.Find("TorpedoButton").Find("Status").GetComponent<Image>();
            content.Find("TorpedoButton").Find("Border").GetComponent<Image>().color = Color.red;
            var button = content.GetComponent<Button>();
            button.enabled = false;
            var color = _resourcesManager.GetTorpedoes(type) <= 0 ? Color.red : Color.green;
            StartCoroutine(ReloadTorpedoTube(tubeNumber, tubeGo, reloadImage, button, color));
            // Включить звук торпеды
            torpedoStartPoint[tubeNumber].GetComponent<AudioSource>().Play();
            // Включить визуализацию торпеды
            var torpedoGo = Instantiate(torpedoPrefab, torpedoStartPoint[tubeNumber].position,
                torpedoStartPoint[tubeNumber].rotation, torpedoHolder);
            torpedoGo.name = "Torpedo";
            torpedoGo.tag = "TorpedoFriendly";
            torpedoGo.GetComponent<Rigidbody>().velocity =
                torpedoStartPoint[tubeNumber].forward * ((int) type + 1) * 10;
            Destroy(torpedoGo, 1 * 60);

            // Добавить объект в игровой менеджер
            GameSceneManager.Instance.ShootTorpedo(type);
        }

        private IEnumerator ReloadTorpedoTube(int tubeNumber, GameObject tubeGo, Image image, Button button,
            Color color)
        {
            var reloadSpeed = _torpedoParameters[torpedoTubes[tubeNumber].type].reloadSpeed;

            while (torpedoTubes[tubeNumber].reloadPercentage < 1f)
            {
                torpedoTubes[tubeNumber].reloadPercentage += torpedoReloadTimeDamp * reloadSpeed;
                image.fillAmount = 1f - torpedoTubes[tubeNumber].reloadPercentage;
                yield return new WaitForSeconds(0.01f);
            }

            tubeGo.transform.Find("Content").Find("TorpedoButton").Find("Border").GetComponent<Image>().color = color;
            button.enabled = true;
            yield return 0;
        }

        public void SpawnEnemyTorpedoHit(RoomController roomController)
        {
            var enemyTorpedo = Instantiate(torpedoPrefab, environmentHolder, true);
            enemyTorpedo.name = "torpedo_hit";
            // Определение стартовой позиции торпеды
            var startPosition = submarineCollider.bounds.center;
            startPosition += (Random.Range(-5.0f, 5.0f) * Vector3.right +
                              Random.Range(1.5f, 5.0f) * Vector3.back).normalized * 100 +
                             Vector3.up * Random.Range(-1.0f, 1.0f) * 50;
            enemyTorpedo.transform.position = startPosition;

            // Определение направленя движения торпеды
            var bounds = roomController.GetComponent<BoxCollider>().bounds;
            var direction = new Vector3(
                Random.Range(bounds.min.x, bounds.max.x),
                Random.Range(bounds.min.y, bounds.max.y),
                Random.Range(bounds.min.z, bounds.max.z)
            );
            enemyTorpedo.transform.LookAt(direction);
            enemyTorpedo.GetComponent<Rigidbody>().velocity =
                (direction - enemyTorpedo.transform.position).normalized * 20;
            Destroy(enemyTorpedo, 60 * 1);

            var etController = enemyTorpedo.AddComponent<EnemyTorpedoController>();
            etController.destination = roomController;
        }

        public void SpawnEnemyTorpedoMiss()
        {
            var enemyTorpedo = Instantiate(torpedoPrefab, environmentHolder, true);
            enemyTorpedo.name = "torpedo_miss";
            enemyTorpedo.GetComponent<BoxCollider>().enabled = false;
            // Определение стартовой позиции торпеды
            var startPosition = submarineCollider.bounds.center;
            startPosition += (Random.Range(-5.0f, 5.0f) * Vector3.right +
                              Random.Range(-5.0f, 5.0f) * Vector3.forward).normalized * 200 +
                             Vector3.up * Random.Range(-1.0f, 1.0f) * 50;
            enemyTorpedo.transform.position = startPosition;
            // Определение направленя движения торпеды
            var direction = submarineCollider.bounds.center +
                            Vector3.right * Random.Range(-1.0f, 1.0f) * (80 / 2 + 10) +
                            Vector3.up * Random.Range(-1.0f, 1.0f) * (10 / 2 + 10);
            enemyTorpedo.transform.LookAt(direction);
            enemyTorpedo.GetComponent<Rigidbody>().velocity =
                (direction - enemyTorpedo.transform.position).normalized * 20;
            Destroy(enemyTorpedo, 60 * 1);
        }

        public void SpawnEnemyTorpedo()
        {
            var enemyTorpedo = Instantiate(torpedoPrefab, environmentHolder, true);
            // Определение стартовой позиции торпеды
            var startPosition = submarineCollider.bounds.center;
            startPosition += (Random.Range(-5.0f, 5.0f) * Vector3.right +
                              Random.Range(-5.0f, 5.0f) * Vector3.forward).normalized * 200 +
                             Vector3.up * Random.Range(-1.0f, 1.0f) * 50;
            enemyTorpedo.transform.position = startPosition;
            // Определение направленя движения торпеды
            var direction = submarineCollider.bounds.center + Vector3.right * Random.Range(-1.0f, 1.0f) * 80 / 2 +
                            Vector3.up * Random.Range(-1.0f, 1.0f) * 10 / 2;
            enemyTorpedo.transform.LookAt(direction);
            enemyTorpedo.GetComponent<Rigidbody>().velocity =
                (direction - enemyTorpedo.transform.position).normalized * 20;
            Destroy(enemyTorpedo, 60 * 1);
        }
    }
}