using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Client.Scripts.Scenes.Game.Submarine
{
    public enum CrewMemberProfession
    {
        // Torpedo

        // Battery
        SeniorElectrician,

        // Living
        Cook,

        // Medical
        Medic,

        // Commander
        Captain,
        ChiefOfficer,

        // Sonar
        WarfareSpecialist,
        CommunicationSpecialist,

        // Energy
        ChiefEngineer,

        // Fuel

        // Engine
        MechanicalOfficer,

        // Storage
        LogisticOfficer,
    }

    public enum CrewMemberOccupation
    {
        Idle,
        Moving,
        ExtinguishingFire,
    }

    [Serializable]
    public class CrewMemberData
    {
        public string name;
        public uint maxHealth;
        public uint initialHealth;
        public Vector3 position;
        public Quaternion rotation;
        public GameObject prefab;
        public Avatar avatar;
        public CrewMemberOccupation occupation;
        public CrewMemberProfession profession;
        public Sprite icon;
    }

    public class CrewManager : MonoBehaviour
    {
        [Header("Crew")]
        public GameObject crewMemberPrefab;

        public Transform crewHolder;

        private CrewMemberController _selectedCrewMember;

        [Header("UI")]
        public RectTransform crewMemberInfoPanel;

        public RectTransform crewMemberListPanel;
        public GameObject crewMemberUiPrefab;

        [Header("Data")]
        public List<CrewMemberData> crewMemberData = new List<CrewMemberData>();

        private Camera _camera;
        private CameraController _cameraController;

        private void Start()
        {
            crewMemberInfoPanel.gameObject.SetActive(false);

            _camera = Camera.main;
            if (_camera != null)
                _cameraController = _camera.transform.GetComponent<CameraController>();

            foreach (var crewMember in crewMemberData)
            {
                // Создание и настройка иконки списка персонажей
                var cmUiGo = Instantiate(crewMemberUiPrefab, crewMemberListPanel);
                cmUiGo.name = crewMember.name;
                cmUiGo.transform.Find("Content").Find("NamePanel").Find("NameText").GetComponent<Text>().text =
                    cmUiGo.name;
                cmUiGo.transform.Find("Content").Find("Photo").Find("Image").GetComponent<Image>().sprite =
                    crewMember.icon;

                // Создание и настройка персонажа
                var cmGo = Instantiate(crewMemberPrefab, crewHolder);
                var character = Instantiate(crewMember.prefab, cmGo.transform);
                Destroy(character.GetComponent<Animator>());
                character.name = "Mesh";
                cmGo.GetComponent<Animator>().avatar = crewMember.avatar;
                cmGo.name = crewMember.name;
                var nma = cmGo.GetComponent<NavMeshAgent>();
                nma.enabled = false;
                cmGo.transform.localPosition = crewMember.position;
                cmGo.transform.rotation = crewMember.rotation;
                var cmController = cmGo.GetComponent<CrewMemberController>();
                cmController.Initialize(crewMember, cmUiGo);
                nma.enabled = true;

                // Настройка реакции на нажатие кнопки списка
                cmUiGo.GetComponent<Button>().onClick.AddListener(() =>
                {
                    if (_selectedCrewMember == cmController)
                        UnselectCrewMember();
                    else
                        SelectCrewMember(cmController);
                });
            }
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var ray = _camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out var hit, 1000, (int) ~ (uint) LayerMask.GetMask("Room"),
                    QueryTriggerInteraction.Ignore))
                {
                    if (hit.collider.CompareTag("Crew"))
                    {
                        var clickedCrewMember = hit.collider.gameObject.GetComponent<CrewMemberController>();
                        if (clickedCrewMember != _selectedCrewMember)
                            SelectCrewMember(clickedCrewMember);
                        else
                            UnselectCrewMember();
                    }
                    else
                    {
                        if (_selectedCrewMember != null && !_cameraController.WasMoved)
                        {
                            _selectedCrewMember.GetComponent<CrewMemberController>().GoTo(hit.point);
                            UnselectCrewMember();
                        }
                    }
                }
            }
        }

        private void SelectCrewMember(CrewMemberController controller)
        {
            UnselectCrewMember();

            _selectedCrewMember = controller;
            _selectedCrewMember.Select();

            crewMemberInfoPanel.gameObject.SetActive(true);
            crewMemberInfoPanel.Find("NameText").GetComponent<Text>().text = _selectedCrewMember.name;
            crewMemberInfoPanel.Find("ProfessionText").GetComponent<Text>().text =
                _selectedCrewMember.profession.ToString();
            crewMemberInfoPanel.Find("Character").Find("Photo").GetComponent<Image>().sprite = _selectedCrewMember.icon;
        }

        private void UnselectCrewMember()
        {
            crewMemberInfoPanel.gameObject.SetActive(false);

            if (_selectedCrewMember == null)
                return;

            _selectedCrewMember.Unselect();
            _selectedCrewMember = null;
        }
    }
}