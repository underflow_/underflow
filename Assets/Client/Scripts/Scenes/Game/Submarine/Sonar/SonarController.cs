﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Scenes.Game.Submarine.Sonar
{
    public enum SonarObjectType
    {
        Submarine,
        Torpedo,
        EnemyTorpedo,
        FriendlyTorpedo,
    }

    public class SonarObject
    {
        public Vector2 Position;
        public Vector2 Rotation;
        public SonarObjectType Type;
    }

    public class SonarController : MonoBehaviour
    {
        [Header("Objects")]
        public GameObject enemyUndefinedPrefab;

        public GameObject enemySubmarinePrefab;
        public GameObject enemyTorpedoPrefab;
        public GameObject friendlyTorpedoPrefab;

        private Vector2 _playerPosition = Vector2.zero;
        private readonly Dictionary<Guid, SonarObject> _objects = new Dictionary<Guid, SonarObject>();

        public float sonarRadius = 3;

        private Transform _holderObject;
        private Transform _spotlightObject;

        private float _spotlightAngle;

        [Range(0.01f, 100)]
        public float spotlightSpeed;

        private const int SpotlightSpeedFactor = 5;

        private float _radarPixelSize;

        #region Contour

        [Header("Contour")]
        public Vector2 contourMovementDirection;

        public float contourMovementSpeed;
        private RectTransform _contourObject;

        #endregion

        private void Start()
        {
            if (!enemyUndefinedPrefab || !enemySubmarinePrefab || !enemyTorpedoPrefab)
            {
                Debug.LogError("Assign a Texture in the inspector.");
            }

            _spotlightObject = transform.Find("Content").Find("Spotlight");
            _holderObject = transform.Find("Content").Find("Objects");
            _contourObject = transform.Find("Content").Find("BackgroundImage").Find("ContourImage")
                .GetComponent<RectTransform>();

            _radarPixelSize = transform.Find("Content").Find("BackgroundImage").transform.GetComponent<RectTransform>()
                .rect.width;
        }

        private void FixedUpdate()
        {
            // Сдвинуть фон
            _contourObject.Translate(contourMovementDirection.normalized * contourMovementSpeed * Time.deltaTime);

            // Изменить угол прожектора
            var oldAngle = _spotlightAngle;
            var angleDiff = spotlightSpeed * SpotlightSpeedFactor * Time.deltaTime;
            _spotlightAngle = (_spotlightAngle + angleDiff) % 360;
            _spotlightObject.transform.Rotate(0, 0, -angleDiff, Space.Self);

            var closeObjects = _objects.Where(@object =>
                Vector2.Distance(_playerPosition, @object.Value.Position) <= sonarRadius
            );

            foreach (var pair in closeObjects)
            {
                var closeObject = pair.Value;

                // Получение относительной позиции объекта
                var relativePosition = (closeObject.Position - _playerPosition) * _radarPixelSize / 2 / sonarRadius;

                // Получение угла относительно субмарины
                var objectAngle = Vector3.Angle(Vector3.up, relativePosition);

                if (-Vector3.up.x * relativePosition.y + Vector3.up.y * relativePosition.x < 0)
                    objectAngle = 360 - objectAngle;

                if (oldAngle <= objectAngle && objectAngle < _spotlightAngle)
                {
                    GameObject prefab;
                    switch (closeObject.Type)
                    {
                        case SonarObjectType.Submarine:
                            prefab = enemySubmarinePrefab;
                            break;
                        case SonarObjectType.Torpedo:
                        case SonarObjectType.EnemyTorpedo:
                            prefab = enemyTorpedoPrefab;
                            break;
                        case SonarObjectType.FriendlyTorpedo:
                            prefab = friendlyTorpedoPrefab;
                            break;
                        default:
                            prefab = enemyUndefinedPrefab;
                            break;
                    }

                    var @object = Instantiate(prefab, _holderObject, false);
                    @object.transform.localPosition = new Vector3(relativePosition.x, relativePosition.y);
                    // TODO: Починить поворот объекта согласно вектору
//                    @object.transform.Rotate(0, 0, closeObject.Rotation * 360 % 360);
                    @object.name = closeObject.Type.ToString();
                    var lifeDuration = 360 / (spotlightSpeed * SpotlightSpeedFactor);
                    Destroy(@object, lifeDuration);
                    var image = @object.transform.Find("Sprite").GetComponent<Graphic>();
                    if (image != null)
                        StartCoroutine(SonarObjectFade(image, lifeDuration, 0.25f));
                }
            }
        }

        private static IEnumerator SonarObjectFade(Graphic image, float time, float percentage)
        {
            percentage = percentage % 1;
            for (var f = 0f; f <= percentage; f += 0.1f)
            {
                var c = image.color;
                c.a = 1 - f;
                image.color = c;
                yield return new WaitForSeconds(time * 0.1f);
            }
        }

        public void UpdatePlayerPosition(Vector2 newPosition)
        {
            _playerPosition = newPosition;
        }

        public void AddObject(Guid id, SonarObject sonarObject)
        {
            _objects[id] = sonarObject;
        }

        public void RemoveObject(Guid id)
        {
            _objects.Remove(id);
        }
    }
}