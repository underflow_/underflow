using System;
using System.Collections.Generic;
using System.Linq;
using Client.Scripts.Scenes.Game.Environment;
using UnityEngine;

namespace Client.Scripts.Scenes.Game.Submarine
{
    [RequireComponent(typeof(BoxCollider), typeof(AudioSource))]
    public class RoomController : MonoBehaviour
    {
        // Название комнаты
        public string Name { get; private set; }

        // Иконка комнаты
        public Sprite Icon { get; private set; }

        // Текущее состояние комнаты
        private RoomState _state;

        public RoomState State
        {
            get => _state;
            set
            {
                _state = value;
                switch (_state)
                {
                    case RoomState.Broken:
                        BreakdownLevel = 1.0f;
                        break;
                    case RoomState.Flooded:
                        break;
                    case RoomState.OnFire:
                        break;
                    case RoomState.Stable:
                        // Восстановить стабильность комнаты
                        switch (type)
                        {
                            case RoomType.Battery:
                                ResourcesManager.Instance.EnergyGenerationEnabled = true;
                                break;
                        }

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        // Скорость починки комнаты
        private const float RepairSpeed = 0.25f;

        // Уровень поломки комнаты
        private float _breakdownLevel = 0.0f;

        private float BreakdownLevel
        {
            get => _breakdownLevel;
            set
            {
                _breakdownLevel = Mathf.Clamp(value, 0f, 1f);
                SetSparksIntensity(_breakdownLevel);
                if (_breakdownLevel <= 0.0f)
                {
                    _breakdownLevel = 0.0f;
                    ResourcesManager.Instance.CurrentSurvivability +=
                        0.05f * ResourcesManager.Instance.maxSurvivability;
                    State = RoomState.Stable;
                }
            }
        }

        public RoomType type;

        private readonly List<Transform> _sparks = new List<Transform>();

        private const float CrewEnergyGeneration = 5.0f;

        private AudioSource _audioSource;

        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();

            // Получить список всех искр
            foreach (Transform child in transform)
                if (child.CompareTag("Sparks"))
                {
                    _sparks.Add(child);
                    child.GetComponent<SparksController>().SetIntensity(0f);
                }
        }

        /// <summary>
        /// Инициализация данных о комнате
        /// </summary>
        /// <param name="data">Данные комнаты</param>
        public void Initialize(RoomData data)
        {
            Name = data.name;
            State = data.state;
            Icon = data.icon;
            type = data.type;
        }

        private void Update()
        {
            if (State == RoomState.Broken)
            {
                if (ResourcesManager.Instance.GetAllCrewInRoom(this).Any())
                {
                    BreakdownLevel -= Time.deltaTime * RepairSpeed;
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Crew"))
            {
                var cmController = other.GetComponent<CrewMemberController>();

                if (ResourcesManager.Instance.crewPresence.ContainsKey(cmController))
                {
                    // Сбросить старое присутсвие
                    switch (ResourcesManager.Instance.crewPresence[cmController].type)
                    {
                        case RoomType.Energy:
                            ResourcesManager.Instance.AdditionalEnergyGenerationSpeed -= CrewEnergyGeneration;
                            break;
                        case RoomType.Undefined:
                            break;
                    }
                }

                // Применить новое присутсвие
                switch (type)
                {
                    case RoomType.Energy:
                        ResourcesManager.Instance.AdditionalEnergyGenerationSpeed += CrewEnergyGeneration;
                        break;
                    case RoomType.Undefined:
                        break;
                }

                // Обновить список присутсвия
                ResourcesManager.Instance.crewPresence[cmController] = this;
            }
            else if (other.CompareTag("Torpedo"))
            {
                var controller = other.GetComponent<EnemyTorpedoController>();
                if (controller == null)
                    return;

                if (controller.destination == this)
                {
                    // Перевести комнату в неисправное состояние
                    State = RoomState.Broken;

                    // Оказать влияние на состояние лодки
                    switch (type)
                    {
                        case RoomType.Engine:
                            break;
                        case RoomType.Energy:
                            ResourcesManager.Instance.CurrentEnergy -= 0.1f * ResourcesManager.Instance.maxEnergy;
                            break;
                        case RoomType.Com:
                            break;
                        case RoomType.Living:
                            break;
                        case RoomType.Torpedo:
                            break;
                        case RoomType.Battery:
                            ResourcesManager.Instance.EnergyGenerationEnabled = false;
                            // TODO: Заблокировать энергию
                            break;
                        case RoomType.Med:
                            break;
                        case RoomType.Sonar:
                            // TODO: Уменьшить скорость сонара
                            break;
                        case RoomType.Fuel:
                            break;
                        case RoomType.Storage:
                            break;
                        case RoomType.Undefined:
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    // Нанести урон всем членам экипажа внутри
                    ResourcesManager.Instance.GetAllCrewInRoom(this)
                        .ForEach(crewMember => crewMember.Health -= 10.0f);

                    // Уничтожить торпеду
                    Destroy(other.gameObject);
                }

                // Уменьшить объем здоровья корабля
                ResourcesManager.Instance.CurrentSurvivability -= 0.1f * ResourcesManager.Instance.maxSurvivability;
            }
        }

        /// <summary>
        /// Установить интенсивность искр
        /// </summary>
        /// <param name="intensityPercentage"></param>
        private void SetSparksIntensity(float intensityPercentage)
        {
            _sparks.ForEach(spark => spark.GetComponent<SparksController>().SetIntensity(intensityPercentage));
        }

        public void MuteAudio()
        {
            _audioSource.mute = true;
        }

        public void UnmuteAudio()
        {
            _audioSource.mute = false;
        }
    }
}