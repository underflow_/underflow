﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client.Scripts.Context;
using Client.Scripts.Scenes.Game.Submarine.Visuals;
using UnityEngine;
using UnityEngine.UI;

namespace Client.Scripts.Scenes.Game.Submarine
{
    [Serializable]
    public class AmmunitionData
    {
        public TorpedoType type;
        public uint amount;
    }

    public class ResourcesManager : MonoBehaviour
    {
        #region Singleton

        public static ResourcesManager Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        [Header("UI")]
        public GameObject survivabilityBar;

        public GameObject energyBar;
        public GameObject speedBar;
        private Image _survivabilityImage;
        private Image _energyImage;
        private Image _speedImage;

        private ScrewController _screwController;

        [Header("Resources")]
        public List<AmmunitionData> ammunitionData = new List<AmmunitionData>();

        #region Crew Presence

        // Словарь присуствия членов экипажа
        public Dictionary<CrewMemberController, RoomController> crewPresence =
            new Dictionary<CrewMemberController, RoomController>();

        /// <summary>
        /// Получить всех членов экипажа в комнате
        /// </summary>
        /// <param name="roomController">Контроллер комнаты</param>
        /// <returns>Список всех членов экипажа в комнате</returns>
        public List<CrewMemberController> GetAllCrewInRoom(RoomController roomController)
        {
            return crewPresence.Where(pair => pair.Value == roomController)
                .Select(pair => pair.Key).ToList();
        }

        public RoomController GetRoomOfCrew(CrewMemberController crewController)
        {
            return crewPresence.TryGetValue(crewController, out var value) ? value : null;
        }

        #endregion

        #region Survivability

        [Header("Survivability")]
        public float initialSurvivability;

        public float maxSurvivability = 100;
        private float _currentSurvivability;

        public float CurrentSurvivability
        {
            get => _currentSurvivability;
            set
            {
                _currentSurvivability = Mathf.Clamp(value, 0.0f, maxSurvivability);
                UpdateBarValue(_survivabilityImage, _currentSurvivability, maxSurvivability);
                if (_currentSurvivability <= 0.0f)
                {
                    GameSceneManager.Instance.Mode = GameMode.Menu;
                    GameManager.Instance.LoadDefeatScene();
                }
            }
        }

        #endregion

        #region Energy

        [Header("Energy")]
        public float initialEnergy;

        public float maxEnergy = 100;

        private float _currentEnergy;

        private bool _energyGenerationEnabled = true;

        public bool EnergyGenerationEnabled
        {
            get => _energyGenerationEnabled;
            set
            {
                _energyGenerationEnabled = value;
                if (!_energyGenerationEnabled)
                {
                    _energyModifierUp.SetActive(false);
                    _energyModifierDown.SetActive(false);
                    _energyModifierStop.SetActive(true);
                }
                else
                {
                    if (_energyModifierStop != null) _energyModifierStop.SetActive(false);
                }
            }
        }

        public float baseEnergyGenerationSpeed;
        private float _additionalEnergyGenerationSpeed;

        public float AdditionalEnergyGenerationSpeed
        {
            get => _additionalEnergyGenerationSpeed;
            set
            {
                _additionalEnergyGenerationSpeed = value;
                if (EnergyGenerationEnabled)
                {
                    if (_additionalEnergyGenerationSpeed > 0.01f)
                    {
                        _energyModifierUp.SetActive(true);
                        _energyModifierDown.SetActive(false);
                    }
                    else if (Math.Abs(_additionalEnergyGenerationSpeed) < 0.01f)
                    {
                        _energyModifierUp.SetActive(false);
                        _energyModifierDown.SetActive(false);
                    }
                    else if (_additionalEnergyGenerationSpeed < 0.01f)
                    {
                        _energyModifierUp.SetActive(false);
                        _energyModifierDown.SetActive(true);
                    }
                }
            }
        }

        private float EnergyGenerationSpeed
        {
            get
            {
                if (EnergyGenerationEnabled)
                    return Mathf.Clamp(baseEnergyGenerationSpeed + AdditionalEnergyGenerationSpeed,
                        0.0f, float.MaxValue);
                return 0.0f;
            }
        }

        public float CurrentEnergy
        {
            get => _currentEnergy;
            set
            {
                _currentEnergy = Mathf.Clamp(value, 0.0f, maxEnergy);
                UpdateBarValue(_energyImage, _currentEnergy, maxEnergy);
            }
        }

        public Color energyModifierColor;

        private GameObject _energyModifierUp;

        private GameObject _energyModifierDown;

        private GameObject _energyModifierStop;

        #endregion

        #region Speed

        [Header("Speed")]
        public float initialSpeed;

        public float maxSpeed = 100;

        private float _currentSpeed;

        public float speedGenerationSpeed;

        public float CurrentSpeed
        {
            get => _currentSpeed;
            set
            {
                _currentSpeed = Mathf.Clamp(value, 0.0f, maxSpeed);
                UpdateBarValue(_speedImage, _currentSpeed, maxSpeed);
                if (ScrewController.Instance != null)
                    ScrewController.Instance.ScrewSpeedPercentage = _currentSpeed / maxSpeed;
            }
        }

        public Color speedModifierColor;

        private GameObject _speedModifierUp;
        private GameObject _speedModifierDown;
        private GameObject _speedModifierStop;

        #endregion

        #region Modifiers

        [Header("Modifiers")]
        public GameObject upModifier;

        public GameObject downModifier;
        public GameObject stopModifier;

        #endregion

        private void Start()
        {
            _screwController = FindObjectOfType<ScrewController>();

            // Получить бары для изменений
            _survivabilityImage =
                survivabilityBar.transform.Find("SurvivabilityBar").Find("Value").GetComponent<Image>();
            _energyImage = energyBar.transform.Find("EnergyBar").Find("Value").GetComponent<Image>();
            _speedImage = speedBar.transform.Find("SpeedBar").Find("Value").GetComponent<Image>();

            // Инициализация модификаторов
            var energyModifierHolder = energyBar.transform.Find("Modifier");
            _energyModifierUp = CreateModifier(energyModifierHolder, upModifier, energyModifierColor);
            _energyModifierDown = CreateModifier(energyModifierHolder, downModifier, energyModifierColor);
            _energyModifierStop = CreateModifier(energyModifierHolder, stopModifier, energyModifierColor);

            var speedModifierHolder = speedBar.transform.Find("Modifier");
            _speedModifierUp = CreateModifier(speedModifierHolder, upModifier, speedModifierColor);
            _speedModifierDown = CreateModifier(speedModifierHolder, downModifier, speedModifierColor);
            _speedModifierStop = CreateModifier(speedModifierHolder, stopModifier, speedModifierColor);

            // Задать начальные значения
            CurrentSurvivability = initialSurvivability;
            CurrentEnergy = initialEnergy;
            CurrentSpeed = initialSpeed;
        }

        /// <summary>
        /// Создание объекта модификатора
        /// </summary>
        /// <param name="holder">Объект для содержания модификатора</param>
        /// <param name="prefab">Префаб модификатора</param>
        /// <param name="color">Цвет модификатора</param>
        /// <returns>Объект модификатора</returns>
        private static GameObject CreateModifier(Transform holder, GameObject prefab, Color color)
        {
            var result = Instantiate(prefab, holder, false);
            result.SetActive(false);
            result.transform.GetComponent<Image>().color = color;
            return result;
        }

        private void Update()
        {
            if (GameSceneManager.Instance.IsRunning)
            {
                CurrentEnergy += EnergyGenerationSpeed * Time.deltaTime;
                CurrentSpeed += speedGenerationSpeed * Time.deltaTime;
            }
        }

        /// <summary>
        /// Использование торпеды
        /// </summary>
        /// <param name="type">Тип торпеды для использования</param>
        /// <returns>Результат использования торпеды</returns>
        public bool UseTorpedo(TorpedoType type)
        {
            // Проверка существования торпед данного типа
            var torpedoes = ammunitionData.Find(_ => _.type == type);
            if (torpedoes == null)
                return false;
            // Проверка наличия торпед данного типа
            if (torpedoes.amount <= 0)
                return false;
            // Проверка наличия энергии
            var requiredEnergy = ((uint) type + 1) * 20;
            if (CurrentEnergy < requiredEnergy)
                return false;
            // Использование торпеды
            CurrentEnergy -= requiredEnergy;
            torpedoes.amount -= 1;
            return true;
        }

        /// <summary>
        /// Получить число торпед
        /// </summary>
        /// <param name="type">Тип требуемых торпед</param>
        /// <returns>Число торпед заданного типа</returns>
        public uint GetTorpedoes(TorpedoType type)
        {
            var torpedoes = ammunitionData.Find(_ => _.type == type);
            return torpedoes?.amount ?? 0;
        }

        /// <summary>
        /// Обновление значения бара
        /// </summary>
        /// <param name="barImage">Изображение бара</param>
        /// <param name="currentValue">Текущее значение</param>
        /// <param name="maxValue">Максимальное значение</param>
        private static void UpdateBarValue(Image barImage, float currentValue, float maxValue)
        {
            barImage.fillAmount = Mathf.Clamp(currentValue / maxValue, 0.0f, 1.0f);
        }
    }
}