﻿using System.Collections;
using Client.Scripts.Scenes.Game.Submarine.Visuals;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Client.Scripts.Scenes.Game.Submarine
{
    [RequireComponent(typeof(NavMeshAgent), typeof(Animator))]
    public class CrewMemberController : MonoBehaviour
    {
        private NavMeshAgent _agent;
        private Animator _animator;

        public Transform destination;

        private float _hatchMovementDamp = 1.0f;
        public float hatchMovementTime = 1.0f;

        private bool _isUsingHatch = false;
        private bool _isClimbingLadder = false;

        private string _name;
        public CrewMemberProfession profession;
        public Sprite icon;

        #region Health

        private float _maxHealth;
        private float _currentHealth;

        public float Health
        {
            get => _currentHealth;
            set
            {
                _currentHealth = Mathf.Clamp(value, 0.0f, _maxHealth);
                // Обновить UI
                if (_healthBar != null)
                    _healthBar.fillAmount = Mathf.Clamp(_currentHealth / _maxHealth, 0.0f, 1.0f);

                // Отправть в мед отсек
                if (_currentHealth / _maxHealth < 0.05f)
                    _agent.SetDestination(
                        FindObjectOfType<RoomManager>().GetRoomOfType(RoomType.Med).transform.position);
            }
        }

        private Image _healthBar;

        #endregion

        private static readonly int SpeedAnimationHash = Animator.StringToHash("Speed");

        private void Start()
        {
            _agent = GetComponent<NavMeshAgent>();
            _animator = GetComponent<Animator>();

            if (destination != null)
                GoTo(destination.position);
        }

        public void Initialize(CrewMemberData data, GameObject listUiObject)
        {
            _healthBar = listUiObject.transform.Find("Content").Find("HealthBar").Find("Value").GetComponent<Image>();
            if (_healthBar == null)
                Debug.LogWarning("Couldn't find health bar image");

            _name = data.name;
            _maxHealth = data.maxHealth;
            Health = data.initialHealth;
            profession = data.profession;
            icon = data.icon;

            gameObject.name = _name;
        }

        private void Update()
        {
            _animator.SetFloat(SpeedAnimationHash, _agent.velocity.magnitude / _agent.speed);

            // Если агент попал на offMeshLink
            if (_agent.isOnOffMeshLink)
            {
                var data = _agent.currentOffMeshLinkData;

                var ladderController = data.offMeshLink.GetComponent<LadderController>();
                var hatchController = data.offMeshLink.GetComponent<HatchController>();

                if (ladderController != null && !_isClimbingLadder)
                {
                    StartCoroutine(ClimbLadder());
                    _isClimbingLadder = true;
                }

                if (hatchController != null && !_isUsingHatch)
                {
                    StartCoroutine(UseHatch());
                    hatchController.Open();
                    _isUsingHatch = true;
                }
            }
        }

        public void Select()
        {
        }

        public void Unselect()
        {
        }

        public void GoTo(Vector3 pos)
        {
            _agent.SetDestination(pos);
        }

        /// <summary>
        /// Передвижение через люк
        /// </summary>
        /// <returns></returns>
        private IEnumerator UseHatch()
        {
            // TODO: Попробовать применить передвежение по параболе
            // https://github.com/Unity-Technologies/NavMeshComponents/blob/ba4a7ce6e3ae77457d38ec23327539f407ca4822/Assets/Examples/Scripts/AgentLinkMover.cs#L50

            var data = _agent.currentOffMeshLinkData;
            _agent.updateRotation = false;

            var originalDestination = _agent.destination;

            var startPos = _agent.transform.position;
            var endPos = data.endPos;

            var duration = hatchMovementTime * _hatchMovementDamp;

            var t = 0.0f;
            var tStep = 1.0f / duration;
            while (t < 1.0f)
            {
                transform.position = Vector3.Lerp(startPos, endPos, t);
                _agent.destination = transform.position;
                t += tStep * Time.deltaTime;
                yield return null;
            }

            transform.position = endPos;
            _agent.updateRotation = true;
            _agent.CompleteOffMeshLink();
            _agent.destination = originalDestination;
            _isUsingHatch = false;
        }

        /// <summary>
        /// Передвижение по лестнице
        /// </summary>
        /// <returns></returns>
        private IEnumerator ClimbLadder()
        {
            var data = _agent.currentOffMeshLinkData;
            _agent.updateRotation = false;

            var startPos = _agent.transform.position;
            var endPos = data.endPos + Vector3.up * _agent.baseOffset;
            var duration = (endPos - startPos).magnitude / _agent.velocity.magnitude;
            var t = 0.0f;
            var tStep = 1.0f / duration;
            while (t < 1.0f)
            {
                transform.position = Vector3.Lerp(startPos, endPos, t);
                _agent.destination = transform.position;
                t += tStep * Time.deltaTime;
                yield return null;
            }

            transform.position = endPos;
            _agent.updateRotation = true;
            _agent.CompleteOffMeshLink();
            _isClimbingLadder = false;
        }
    }
}