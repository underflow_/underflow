﻿using UnityEngine;

namespace Client.Scripts.Scenes.Game.Submarine.Visuals
{
    public class HatchController : MonoBehaviour
    {
        private Animator _animator;
        
        private static readonly int OpenAnimationHash = Animator.StringToHash("Open");

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public void Open()
        {
            _animator.SetBool(OpenAnimationHash, true);
        }

        public void Close()
        {
            _animator.SetBool(OpenAnimationHash, false);
        }
    }
}