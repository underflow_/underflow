﻿using UnityEngine;

namespace Client.Scripts.Scenes.Game.Submarine.Visuals
{
    public class ScrewController : MonoBehaviour
    {
        #region Singleton

        public static ScrewController Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }

        #endregion

        public Transform screwGameObject;

        private float _screwSpeed;

        public float ScrewSpeed
        {
            get => _screwSpeed;
            set => _screwSpeed = Mathf.Clamp(value, 0.0f, maxSpeed);
        }

        public float ScrewSpeedPercentage
        {
            set
            {
                if (value < 0.0f || value > 1.0f)
                    return;
                ScrewSpeed = value * maxSpeed;
            }
        }

        [Range(0, float.MaxValue)]
        public float maxSpeed;

        private void FixedUpdate()
        {
            screwGameObject.Rotate(0, ScrewSpeed * Time.deltaTime, 0);
        }
    }
}