﻿using UnityEngine;

namespace Client.Scripts.Scenes.Game.Environment
{
    public class SoundController : MonoBehaviour
    {
        public Transform underwaterSound;

        private void Update()
        {
            var cameraPosition = Camera.main.transform.position;
            if (cameraPosition != null)
                underwaterSound.position =
                    new Vector3(cameraPosition.x, cameraPosition.y, underwaterSound.position.z);
        }
    }
}