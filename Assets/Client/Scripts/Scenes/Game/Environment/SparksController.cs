using UnityEngine;

namespace Client.Scripts.Scenes.Game.Environment
{
    public class SparksController : MonoBehaviour
    {
        private Light _lightSource;
        private ParticleSystem _particles;

        private ParticleSystem.EmissionModule _emissionModule;

        private float _maxIntensity;

        private void Awake()
        {
            _lightSource = GetComponent<Light>();
            _particles = GetComponent<ParticleSystem>();

            _emissionModule = _particles.emission;
            _maxIntensity = _emissionModule.rateOverTime.constant;
        }

        /// <summary>
        /// Установить интенсивность искр
        /// </summary>
        /// <param name="intensityPercentage">Процент интенсивности от 0 до 1</param>
        public void SetIntensity(float intensityPercentage)
        {
            // Изменить интенсивность эмиссии
            _emissionModule.rateOverTime = intensityPercentage * _maxIntensity;
            // Включить/выключить источник света
            _lightSource.enabled = !(intensityPercentage <= 0.0f);
        }
    }
}