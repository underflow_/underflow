﻿using System.Collections;
using Client.Scripts.Context;
using Client.Scripts.Scenes.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Client.Scripts.Scenes.Credits
{
    public class CreditsSceneManager : MonoBehaviour
    {
        private void Awake()
        {
            StartCoroutine(LoadSubmarineSceneAsync());
        }

        private IEnumerator LoadSubmarineSceneAsync()
        {
            var asyncLoad = SceneManager.LoadSceneAsync("GameScene", LoadSceneMode.Additive);

            while (!asyncLoad.isDone)
                yield return null;

            FindObjectOfType<CameraController>().viewMode = CameraViewMode.Disabled;
            GameSceneManager.Instance.Mode = GameMode.Menu;
            if (Camera.main != null) Camera.main.transform.Translate(-10, 0, -100);
        }

        public void OnContinueButtonClick()
        {
            GameManager.Instance.LoadMainMenuScene();
        }
    }
}