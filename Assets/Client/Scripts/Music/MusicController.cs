﻿using System;
using System.Collections;
using System.Collections.Generic;
using Client.Scripts.Context;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Client.Scripts.Music
{
    public enum TrackType
    {
        Undefined = 0,
        MainMenuTheme,
        TutorialTheme,
        GameTheme,
        FinalTheme,
    }

    [Serializable]
    public struct TrackEntry
    {
        public TrackType type;
        public AudioClip clip;
    }

    public class MusicController : MonoBehaviour
    {
        public static MusicController Instance = null;

        private AudioSource _audioSource;
        public List<TrackEntry> tracks;
        public float timeToChange;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);

            _audioSource = GetComponent<AudioSource>();

            switch (SceneManager.GetActiveScene().name)
            {
                case "GameScene":
                    _audioSource.clip = GetTrack(TrackType.TutorialTheme);
                    break;
                case "MainMenuScene":
                case "SettingsMenuScene":
                    _audioSource.clip = GetTrack(TrackType.MainMenuTheme);
                    break;
                case "CreditsScene":
                    _audioSource.clip = GetTrack(TrackType.FinalTheme);
                    break;
            }

            _audioSource.enabled = true;
            _audioSource.Play();
        }

        private void Update()
        {
            if (Camera.main != null)
                transform.position = Camera.main.transform.position;
        }

        public void SetTrack(TrackType type)
        {
            var clip = GetTrack(type);
            if (_audioSource.clip == clip)
                return;

            StartCoroutine(ChangeMusic(clip));
        }

        private IEnumerator ChangeMusic(AudioClip clip)
        {
            while (_audioSource.volume > 0)
            {
                _audioSource.volume -= 1 / timeToChange * Time.deltaTime;
                yield return null;
            }

            _audioSource.clip = clip;

            if (Settings.Current.MusicOn)
                _audioSource.Play();

            while (_audioSource.volume < Settings.Current.MusicVolume)
            {
                _audioSource.volume += 1 / timeToChange * Time.deltaTime;
                yield return null;
            }
        }

        private AudioClip GetTrack(TrackType type)
        {
            foreach (var trackEntry in tracks)
            {
                if (trackEntry.type == type)
                    return trackEntry.clip;
            }

            return null;
        }

        public void ChangeState(bool isOn)
        {
            if (isOn)
                _audioSource.UnPause();
            else
                _audioSource.Pause();
        }

        public void ChangeVolume(float volume)
        {
            _audioSource.volume = volume;
        }
    }
}